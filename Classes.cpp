﻿// Classes.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;
class Vector
{
private:
    double x;
    double y;
    double z;
public:
    Vector() : x(3), y(5), z(9)
    {}
    Vector(double _x, double _y, double _z): x(_x),y(_y),z(_z)
    {}
    void Show()
    {
        cout << x << " " << y << " " << z << "\n";
    }

    int Module()
    {
        double m;

        m = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
        cout << m << endl;
        return m; 
    }

};


int main()
{
    Vector v(12,6,18);
    v.Show();
    v.Module();
}


